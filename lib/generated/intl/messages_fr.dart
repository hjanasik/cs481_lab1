import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static m0(name) => "Bonjour $name";

  static m1(firstName, lastName) => "Mon nom est $lastName, $firstName $lastName";

  // ignore: unnecessary_brace_in_string_interps
  static m2(howMany) => "${Intl.plural(howMany, one: 'Vous avez 1 message', other: 'Vous avez ${howMany} messages')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "pageHomeListTitle" : MessageLookupByLibrary.simpleMessage("Quelques chaînes localisées"),
    "pageHomeSamplePlaceholder" : m0,
    "pageHomeSamplePlaceholdersOrdered" : m1,
    "pageHomeSamplePlural" : m2
  };
}
