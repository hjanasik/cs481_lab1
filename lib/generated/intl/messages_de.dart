import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  static m0(name) => "Willkommen $name";

  static m1(firstName, lastName) => "Mein Name ist $lastName, $firstName $lastName";

  // ignore: unnecessary_brace_in_string_interps
  static m2(howMany) => "${Intl.plural(howMany, one: 'Sie haben 1 Nachricht', other: 'Sie haben ${howMany} Nachrichten')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "pageHomeListTitle" : MessageLookupByLibrary.simpleMessage("Einige lokalisierte Zeichenfolgen:"),
    "pageHomeSamplePlaceholder" : m0,
    "pageHomeSamplePlaceholdersOrdered" : m1,
    "pageHomeSamplePlural" : m2
  };
}
