import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(name) => "Welcome $name";

  static m1(firstName, lastName) => "My name is $lastName, $firstName $lastName";

  // ignore: unnecessary_brace_in_string_interps
  static m2(howMany) => "${Intl.plural(howMany, one: 'You have 1 message', other: 'You have ${howMany} messages')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "pageHomeListTitle" : MessageLookupByLibrary.simpleMessage("Some localized strings:"),
    "pageHomeSamplePlaceholder" : m0,
    "pageHomeSamplePlaceholdersOrdered" : m1,
    "pageHomeSamplePlural" : m2
  };
}
