import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'generated/l10n.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    var matApp = new MaterialApp(localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      AppLocal.delegate
    ], 
    supportedLocales: AppLocal.delegate.supportedLocales,
    home: MyHomePage());
    return matApp;
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String user_fname_default = "John";
  String user_lastName_default = "Doe";
  var rng = Random();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              RaisedButton(
                child: Text("Change information"),
                onPressed: () => showDialogInformation(context),
              ),
              RaisedButton(
                child: Text("Click to change the language"),
                onPressed: () => showDialogLanguage(context),
              ),
              Text(AppLocal.of(context).pageHomeListTitle, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
              Text(""),
              Text(AppLocal.of(context).pageHomeSamplePlaceholder(user_fname_default), style: TextStyle(fontSize: 20)),
              Text(AppLocal.of(context).pageHomeSamplePlaceholdersOrdered(user_fname_default, user_lastName_default), style: TextStyle(fontSize: 20)),
              Text(AppLocal.of(context).pageHomeSamplePlural(rng.nextInt(100)), style: TextStyle(fontSize: 20)),
            ]
          ),
        ),
      ),
    );
  }


  void showDialogLanguage(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) =>  AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            RaisedButton(
              child: Text("English"),
              onPressed: () {
                setState(() { AppLocal.load(Locale('en', 'US')); });
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text("German"),
              onPressed: () {
                setState(() { AppLocal.load(Locale('de', 'DE')); });
                Navigator.of(context).pop();
              },
            ),
            RaisedButton(
              child: Text("Fançais"),
              onPressed: () {
                setState(() { AppLocal.load(Locale('fr', 'FR')); });
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      ),
    );
  }

  void showDialogInformation(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextFormField(
              decoration: InputDecoration(
                hintText: "First Name"
              ),
              onChanged: (value) => user_fname_default = value,
            ),
            TextFormField(
              decoration: InputDecoration(
                hintText: "Last Name"
              ),
              onChanged: (value) => user_lastName_default = value,
            ),
          ],
        ),
        actions: [
          FlatButton(
            child: Text("Cancel"),
            onPressed: () => Navigator.of(context).pop(),
          ),
          FlatButton(
            child: Text("Accept"),
            onPressed: () {
              setState(() {});
              Navigator.of(context).pop();
            },
          )
        ],
      )
    );
  }

}
